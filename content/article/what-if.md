---
date: 2016-09-04T17:00:00-06:00
description: What if we gave everyone a company?
draft: true
slug: what-if
title: What if
---
[**Previously**]({{< relref "article/ideas-and-stories.md" >}}) - You can read the previous article in [Ideas & Stories]({{< relref "article/ideas-and-stories.md" >}})

## We have some questions …

What if we gave everyone - a company?

What if we helped them reach markets, clients, providers, and community? What if we provided support it was needed, and guidance where it matters? What if we could provide opportunities for meaningful transformation? What if we could provide real choice, legitimacy, and hope? 

What if we did that all, from the palm of your hand?

### Just the beginning

_While we're starting in South Africa, Kabbo is meant for the whole of Africa_

To one set of eyes, South Africa is a nation of high unemployment, and slowing local and international trade. Much of our history has seen wealth moved abroad, and many now face steep obstacles to progress. Income disparity is high, and rising, and education and national service delivery are underperforming. These are troubling economic indicators, and currently don't tell a story of progress, despite our leadership of the continent in many aspects, the population as a whole is still struggling.

Reducing that disparity will require new ways of demolishing the barriers to economic participation. We will need to provide empowerment through services and guidance - without creating complexity.

We think you should be able to start a new business with a simple tap, and run it from the palm of your hand. What would you do, if you could do that - today?



[**Next**]({{< relref "article/a-look-at-south-africa.md" >}}) - You can read more in [A look at South Africa]({{< relref "article/a-look-at-south-africa.md" >}})

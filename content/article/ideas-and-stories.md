---
date: 2015-12-21T04:05:25-06:00
description: Our story, our purpose.
draft: true
featured: true
slug: ideas-and-stories
title: Ideas & Stories
---

Often, organisations like to headline their efforts with a mission statement, or in some cases, describe a philosophy. We believe that beneath any missions or objectives, there lies an organisation's _purpose_. Although we may seek new challenges, or adapt to a changing world, our purpose is unlikely to change. People and organisations are at their best when their purpose is clear, and shared by all parties.

### Our purpose

Our purpose is to guide individuals toward [financial freedom]({{< relref "article/financial-freedom.md" >}}) and empowerment. Our objective is to provide pervasive low-cost/free services that simplify business & trade, and automate compliance. 

We believe we can provide platforms that transform the processes of business, create opportunities that empower, and give Africans the ability to achieve their own successes, and create opportunities for others themselves. We'd like to see Africans prosper, and Africa herself thrive. We'd like to give all South Africans with an idea, and a dream, the ability to make them part of reality.

### 

### Our name

In the early 1800s a young [Khoisan](https://en.wikipedia.org/wiki/Khoisan) boy took the name [//Kabbo](http://www.thejournalist.org.za/pioneers/kabbo-uhi-ddoro-jantje-tooren-rainmaker-storyteller-visionary), meaning 'Dreamer'. He dreamt of telling the stories of his people. We chose the name Kabbo to honour all of our peoples, their dreams, and their stories.

### Our story

A business has simple beginnings. Shared ideas are the power behind an engine that takes goods, services, and skills and transforms them, creating more value for everyone involved. Each and every business is a story, a narrative, an idea that's been made real. 

But _doing_ business is complex. Legalities, capital, staffing, revenue streams, sales funnels - every aspect of growing a business introduces new processes, new ideas, and new rules. For developing nations, there are knowledge, skills, linguistic, and other cultural obstacles. To small businesses, these barriers are either pure _noise_, an interruption of their story - or more often, seem simply impassable.

All the functions of a business: access to markets, payment systems, banking, financing, logistics, legal compliance, etc. are affected by these obstacles. This becomes an artificial constraint on opportunity, and on legitimacy itself. This stalls attempts at progress and prosperity, and thus entire communities and countries stagnate - ultimately affecting us all.

Yet individuals are not lacking will, rather lacking _means_. We simply must provide those means - simplify access to markets, automate key processes, dissolve and lower barriers.  So that's where we'd like to tell our story, and by doing so, help everyone tell theirs.

[**Next**]({{< relref "article/what-if.md" >}}) - You can read more in [What if ...]({{< relref "article/what-if.md" >}})
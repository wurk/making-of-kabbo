---
author: vai
comments: true
date: 2016-08-18T03:15:25+02:00
description: All the .csproj and MSBuild happy fun times, and I mutter about
  message orientation and queues.
draft: false
featured: false
image: /images/devenv_2016-09-26_04-42-53.png
share: true
slug: solution-scaffolding
tags:
- kabbo
- devlog
title: Solution Scaffolding
---

# Scaffolding

I'm creating the initial solution layout and structure to support the backend services for the Kabbo system. The solution is following my standard MQ/n-tier solution layout, which consists of the following Visual Studio projects -

## Projects

![Solution Dependency Graph][solutionDependencyGraph]

**Kernel**  
This is a hybrid console application, supporting both interactive CLI mode, and running as an Windows Service. This enables interactive debugging, and the use of the same executable to perform Windows Service maintenance tasks.

- Literate console logging
- Logs to Elasticsearch sink via RabbitMq
- Logs structured POCO data

**Messages**  
This library contains all the solution services' message type definitions, and their metadata to support serialisation

*   Convention based plus metadata overrides
*   XML / Json / ProtocolBuffers / Jsv / Csv serialisers
*   Versioned
*   Strongly typed client library support

**Model**  
The types and metadata defined within this library are the definition of the persistent domain model for the solution

**Services**  
The actual implementations of the domain's services are contained in this library

**Web**  
Front end services and metadata to support our HTML5 frontend

---

## Dependencies

Building out the Kernel library, so that we can begin working with our service runtime. We'll include some of our default packages now -

**Wurk**  
A base set of configuration management utilities, providing strongly typed, overloadable access to application settings.

**Wurk Fabric**  
Using Fody, this package applies the GitVersion MSBuild task to our library, stamping our compiled assembly with the GitFlow versioning convention we specify. In addition, it'll compile our project's build output as per the .nuspec file's definition into a nuget package, we'll use this come deploy time. This also weaves in Serilog support to our application. +Annotations, `ModuleInit`

**Wurk Data**  
This is our persistence feature library, a standard set of conventions for mapping our domain types to their SQL counterparts, and the NHibernate implementation of those conventions. Additionally, this library supports the generation and application of data migrations.

**Wurk DataServices**  
This library defines some common base class types to support our service conventions for data entities

**Wurk ServiceHost**  
Our base Windows Service library implementation, implementing the Service control patterns, and runtime conventions for our system. Our Service class will derive from this base class.

**Wurk Tools**  
Base utilities for serialisation and support functions for the Wurk practices and patterns.

Having added these packages, we've got a few more pieces of wiring to do. We'll start by defining our Service implementation



[solutionDependencyGraph]: /images/devenv_2016-09-26_05-12-40.png	"Our solution's dependency graph"


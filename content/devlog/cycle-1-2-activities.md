---
author: vai
date: 2016-10-10T14:52:09+02:00
description: A quick presentation covering our development decisions and explorations
draft: false
image: /images/firefox_2016-10-10_14-53-28.png
slug: cycle-1-2
tags:
- Development
- Presentation
- Cycle Report
- devlog
title: Cycle 1 & 2
---

## Development activities
A quick summary of development directions and our explorations, materials prepared for an introduction to a live demo / interactive session held 2016-10-09. 

[**Presentation**](https://kabbo.co/cycle1-2-meeting/): Cycle 1 and 2 - Development Activities is available [**here**](https://kabbo.co/cycle1-2-meeting/)
---
author: Matthew McLeod
date: 2016-08-02T19:10:10+02:00
description: We've registered our submission for incorporation with the CIPC
featured: false
slug: filed-incorporation
tags:
- Milestone
- Legal
- CIPC
title: Filed for incorporation
---

All relevant and required document for the Kabbo corporation has been filed with the [Companies and Intellectual Properties Commission](http://www.cipc.co.za/).
---
author: Matthew McLeod
date: 2016-08-02T19:10:00+02:00
description: The kabbo.co and kabbo.co.za domain names have been registered
featured: false
image: 
slug: domain-registration
tags:
- Milestone
- Domains
- DNS
- Technical Infrastructure
title: Domain Registration
---

`kabbo.co` was registered through [Gandi](https://www.gandi.net/), and `kabbo.co.za` via [Hetzner](https://hetzner.co.za/).
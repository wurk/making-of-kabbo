---
author: Matthew McLeod
date: 2016-08-02T19:11:10+02:00
description: A Gitlab organisation and initial repositories for the Kabbo system have been created and configured
draft: false
featured: false
tags:
- Milestone
- Gitlab
- Source control
- Technical infrastructure
title: Hello, Gitlab!
---

We've created the [Gitlab](https://gitlab.com) organisation and repositories we'll be using for primary development of the system.
---
author: Matthew McLeod
date: 2016-08-05T19:10:00+02:00
description: Kabbo's incorporation documentation was approved. Now an Actual Real Legal Thing.
draft: false
featured: true
tags:
- Milestone
- Legal
- CIPC
slug: kabbo-inc
title: Kabbo Inc.
---

Our incorporation documentation was all in order, and the Companies and Intellectual Properties Commission ([CIPC](http://www.cipc.co.za/)) have replied with our registration details.

`Kabbo (Pty) Ltd` is now registered with the [CIPC](http://www.cipc.co.za/) as `K2016332669`, as is our defensive name `Kabbo`. 

`Kabbo (Pty) Ltd` is now registered with [South African Revenue Services](http://www.sars.gov.za/) as `2016/332669/07`.
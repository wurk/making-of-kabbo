---
author: Matthew McLeod
date: 2016-08-02T19:11:00+02:00
description: A blog for technical and business development 
draft: false
featured: false
image:
tags:
- Milestone
title: Development journal
---

We've implemented a simple Blogger journal at http://devlog.kabbo.co/ so we can post status and development notes, as well as provide technical material for discussion.[^1]



[^1]: _This is now replaced by [this](https://making.kabbo.co) site_


---
author: Matthew McLeod
date: 2016-08-02T19:11:30+02:00
description: Email à la carte
draft: false
featured: false
tags:
- Milestone
- Domains
- DNS
- Email
- Technical infrastructure
- Google Apps
title: Google Apps configured
---

~~Google Apps~~ G Suite services for the `kabbo.co` domain are in place, and virus and spam filtering services are in effect


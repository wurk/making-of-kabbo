---
author: Matthew McLeod
date: 2016-08-02T19:11:20+02:00
description: Kabbo now talks SMTP, IMAP, and NO.
draft: false
featured: false
tags:
- Milestone
- Domains
- DNS
- Email
- Technical infrastructure
- Google Apps
title: Email configured
---

Email for the kabbo.co domain is now secure and online.  [matthew] @kabbo.co now routes to my email. We're making use of ~~Google Apps~~ G Suite services.


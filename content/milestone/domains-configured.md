---
author: Matthew McLeod
date: 2016-08-02T19:10:30+02:00
description: Domains configured to point to our alpha environment
draft: false
featured: false
image: null
tags:
- Milestone
- Domains
- DNS
- Technical Infrastructure
title: Domains Configured
---

Our domains and hosts have been configured for our alpha / omicron environment.
---
author: Matthew McLeod
date: 2016-08-02T19:10:40+02:00
description: Our CDN, WAF, and (temporary) SSL solution have been implemented via Cloudflare
draft: false
featured: false
image: null
tags:
- Milestone
- CDN
- WAF
- Cloudflare
- Domains
- Technical Infrastructure
title: CDN,  WAF & SSL implemented
---

We've used Cloudflare's full proxy solution to provide end-user encryption via `HTTPS` & `SSL` . This also provides us geographically distributed end points for static content delivery via the Cloudflare CDN, and their basic Web Application Firewall system. This mitigates some level of browser scripting XSS attempts, and direct server abuse via DDOS and other means.

This is a temporary solution until we purchase wildcard SSL for the `kabbo.co` domain, as we'll require this for user sites. At that time we'll implement end-to-end encryption, including client certificates for Cloudflare itself, thus allowing only one legitimate traffic source to our own infrastructure.
---
author: Matthew McLeod
date: 2016-08-02T19:10:20+02:00
description: Our reservation of Kabbo was successful
draft: false
featured: false
slug: defensive-name-reserved
tags:
- Milestone
- Legal
- CIPC
title: Defensive Name reserved
---

Our application to reserve the name `Kabbo` in South Africa with the [Companies and Intellectual Properties Commission](http://www.cipc.co.za/) was successful. On successful incorporation (still pending), this name will be ours to use nationally.    
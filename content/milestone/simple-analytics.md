---
author: Matthew McLeod
date: 2016-08-02T19:10:50+02:00
description: Preliminary analytics for live events and client technology demographics implemented via Gaug.es
draft: false
featured: false
tags:
- Milestone
- Analytics
title: Simple analytics added
---

We've added the [Gaug.es](http://gaug.es/) tracking script to the Kabbo home page so as to allow us to gather some initial metrics around client technologies.
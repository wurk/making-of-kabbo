---
author: Matthew McLeod
date: 2016-08-02T19:11:40+02:00
description: HTTPS, now with a touch more security.
draft: false
featured: false
tags:
- Milestone
- Domains
- DNS
- HSTS
- Technical infrastructure
title: HSTS implemented
---

[http://kabbo.co](http://kabbo.co/) now implements HSTS, and insecure HTTP clients will have their connection upgraded, as well as remembering the domain as a secure-only site

